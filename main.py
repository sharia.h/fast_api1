from typing import List
from fastapi import Depends, FastAPI, HTTPException, Request
from sqlalchemy.orm import Session
import crud
from database import SessionLocal, engine
import models
import schemas

# uvicorn main:app --reload
models.Base.metadata.create_all(bind=engine)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

@app.post("/insert-country")
async def add_country(request: Request, db: Session = Depends(get_db)):
    db_country = await request.json()
    if not db_country:
        raise HTTPException(status_code=400, detail="Something went wrong")
    return crud.create_country(db=db, info=db_country)

@app.post("/insert-countryneighbour/")
async def add_country_neighbour(request: Request, db: Session = Depends(get_db)):
    db_country = await request.json()
    if not db_country:
        raise HTTPException(status_code=400, detail="Something went wrong")
    return crud.create_country_neighbour(db=db, info=db_country)


@app.get("/country")
async def read_country(request: Request, db: Session = Depends(get_db)):
    country = crud.get_country(db, request=request)
    return country

@app.get("/country/{id}")
def read_country_by_id(id: int, db: Session = Depends(get_db)):
    country_id = crud.get_country_by_id(db, id=id)
    if country_id is None:
        raise HTTPException(status_code=404, detail="User not found")
    return country_id

@app.get("/country/{id}/neighbour")
def read_country_neighbour(id: int, db: Session = Depends(get_db)):
    country = crud.get_country_neighbour(db, id=id)
    return country

